let url = new URL(document.URL);
let search_params = url.searchParams; 
let lgout = search_params.get('lgout');
let login = search_params.get('login');
let hData = search_params.get('hData');
let logup = search_params.get('logup');
if (lgout) {
	alert('Selamat kamu berhasil logout!');
	window.location.replace("login.php");
}
if (login) {
	alert('Selamat kamu berhasil login!');
	window.location.replace("index.php");
}
if (logup) {
	alert('Selamat kamu berhasil mendaftar!');
	window.location.replace("login.php");
}
if (hData) {
	alert('Selamat data berhasil direset!');
	window.location.replace("login.php");
}
if (true) {}

function signup() {
	let pindah = confirm("Yakin data sudah benar?");
	if (pindah) {
		window.location.replace("login.php?logup=true");
	}
}

function signin() {
	let pindah = confirm("Yakin data sudah benar?");
	if (pindah) {
		window.location.replace("index.php?login=true");
	}
}

function reset() {
	let pindah = confirm("Yakin data ingin direset?");
	if (pindah) {
		document.getElementById('user').value = "";
		document.getElementById('pw').value = "";
	}
}