<?php 
session_start();
include("function.php");
if (isset($_POST['login'])) {
	$data = file_get_contents("database/datauser.txt"); 
	$arr1 = explode("^" , $data);
	$banyakArr = count($arr1);
	$salah = 0;

	for ($i=0; $i <$banyakArr-1 ; $i++) { 
		$arr2 = explode("|" , $arr1[$i]);
		if ($arr2[6] == $_POST['user'] && $arr2[4] == $_POST['pw']) {
			increment();
			if ($arr2[1] == 0) {
				$_SESSION['status'] = 'admin';
			}else {
				$_SESSION['status'] = 'user';
			}
			header('Location: index.php?login=true');
			exit;
		}else {
			$salah++;
		}
	}
	if ($salah == $banyakArr-1) {
		echo '<script type="text/javascript">alert("Username/Password  Salah!!")</script>';
	}
}

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/sign.css">
	<script type="text/javascript" src="js/script.js"></script>
</head>
<body>
<div class="wrapper-sign">
	<a href="#"><h1 style="color: #00b3ff">Sign In</h1></a>
	<a href="pendaftaran.php"><h1 >Sign Up</h1></a>
	<form action="" method="POST">
		<div style="text-align: center;">
			<input required class="hundpers" placeholder="Username" type="text" name="user" id="user">
			<input required class="hundpers" placeholder="Password" type="password" name="pw" id="pw">
		</div>
		<div class="kiri-sign"><input type="checkbox" name="robot"> Simpan kata sandi</div><br>
	<center><input class="btn-sign" type="submit" name="login"></form></center>
	<center><button style="background-color: #00b3ff" class="btn-sign" onclick="return reset()">Reset</button></center>
	<div style="text-align: center; margin-top: 1em;">
		Lupa password? <a style="color: lightblue; text-decoration: underline;" href="http://google.com/404">Reset disini</a>
		<br>
		Belum punya akun? <a style="color: lightblue; text-decoration: underline;" href="pendaftaran.php">Daftar disini</a>
	</div>
</div>
</body>
</html>
