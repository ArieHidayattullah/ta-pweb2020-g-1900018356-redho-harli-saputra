<?php 
session_start();
include 'function.php';


 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://kit.fontawesome.com/8029758e2c.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,100;1,500&display=swap" rel="stylesheet">
</head>
<body>
	<?php include 'navbar.php'; ?>
	<section class="banner">
		<div class="content">
			<h1>I'M REDHO H.S</h1>
			<h2>Just a normal person</h2>
		</div>
	</section>
	<section id="about" class="about">
		<h1>ABOUT ME</h1>
		<h2>Let's get to know myself more deeply</h2>
		<!-- <div style="height: .3em; background-color: white; margin: 1em auto; width: 10%; border-radius: 1em;"></div> -->
		<div class="content">
			<img src="https://scontent.fcgk9-2.fna.fbcdn.net/v/t1.0-9/72991500_2605022012912525_7333088700034711552_n.jpg?_nc_cat=104&_nc_sid=09cbfe&_nc_eui2=AeGhWtGW7XiJ3CQYaGT0KAPegxyItgmMuYSDHIi2CYy5hJhojMqSWqxkI1l71Rf3vRh6Pkb93upu95OvqOn5FMUX&_nc_ohc=wVQzxDOyj-gAX_Z1u5y&_nc_ht=scontent.fcgk9-2.fna&oh=0e2e79213fe5e7abe9326260526e123a&oe=5F1DFDBA">
			<p>My name is Redho Harli Saputra, born in Bengkulu precisely on July 25, 2001. Currently I am approximately 19 years old and at my age I am now studying at one of the universities in Jogjakarta, UAD. at UAD I majored in information engineering because I was very interested in the world of programming but even though I was interested in programming I still didn't like mathematics. my hobby is playing games and of course programming. then my best achievement right now is getting an MoE at the 2019 LKSN in Jogjakarta and keeping 8th out of 32 provinces.</p></div>
	</section>
	<section id="skill" class="skill">
		<h1>TECHNICAL SKILLS</h1>
		<h2>The skill I'm good at</h2>

		<div class="content">
			<ul>
				<li>
					<h2>50%</h2>
					<p>HTML</p>
				</li>
				<li>
					<h2>50%</h2>
					<p>CSS</p>
				</li>
				<li>
					<h2>50%</h2>
					<p>PHP</p>
				</li>
				<li>
					<h2>50%</h2>
					<p>JS</p>
				</li>
			</ul>
		</div>
	</section>
	<section id="work" class="work" style="">
		<h1>WORK EXPERIENCE</h1>
		<h2>My work experiences</h2>

		<div class="content" >
				<div class="left">
					<p style="float: right; margin-right: 1em; padding: 1em; border-radius: 50%; width: 5em; height: 5em; background-color: white; color: black"><span style="position: relative; top: 1.5em; ">May 2019</span></p>
				</div>
				<div class="right">
					<h2 class="judul">Corien Centre</h2>
					<p class="subjudul">Computer Technician</p>
					<p class="isi">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
		</div>

		
		</div>
	</section>
	<?php include 'footer.php'; ?>
</body>
</html>