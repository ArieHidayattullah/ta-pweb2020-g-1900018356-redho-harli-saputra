<?php 
session_start();
if(!isset($_SESSION['status'])){
	header("Location: index.php");
}
include 'function.php';

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>User Area</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://kit.fontawesome.com/8029758e2c.js" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@1,100;1,500&display=swap" rel="stylesheet">
	<style type="text/css">
		table {
		    border: solid 1px #DDEEEE;
		    border-collapse: collapse;
		    border-spacing: 0;
		    width: 80%;
		    background-color: white;
		    color: black;
		    font: normal 13px Arial, sans-serif;
		    margin: 0 auto;
		}
		table thead th {
		    background-color: #DDEFEF !important;
		    border: solid 1px #DDEEEE !important;
		    color: #336B6B;
		    padding: 10px;
		    text-align: left;
		    text-shadow: 1px 1px 1px #fff;
		}
		table tbody td {
		    border: solid 1px #DDEEEE;
		    color: #333;
		    padding: 10px;
		    text-shadow: 1px 1px 1px #fff;
		}
	</style>
</head>
<body>
	<nav>
		<div class="right">
			<ul>
				<li><a href="user.php">User Area</a></li>
				<li>||</li>
				<li><a href="index.php">Back to home</a></li>
			</ul>
		</div>
	</nav>

	<section>
		<h1>List User</h1>
		<table width="100%">
			<thead>
				<th width="5%">No.</th>
				<th>Username</th>
				<th>Nama</th>
				<th>E-Mail</th>
				<th>No. HP</th>
			</thead>
				<?php 
				$no = 1;
				$data = file_get_contents("database/datauser.txt"); 
				$arr1 = explode("^" , $data);
				$banyakArr = count($arr1);
				$salah = 0;

				for ($i=0; $i <$banyakArr-1 ; $i++) { 
					$arr2 = explode("|" , $arr1[$i]);
					echo "<tr>

					<td>".$no."</td>
					<td>".$arr2[6]."</td>
					<td>".$arr2[2]."</td>
					<td>".$arr2[3]."</td>
					<td>".$arr2[5]."</td>
					</tr>";
					$no++;
				}

				 ?>
		</table>
	</section>

	<section style="font-family: Roboto">
		<h1>Upload IMG</h1>
		<form action="" method="post" enctype="multipart/form-data">
        <input type="file" name="berkas" />
        <input class="btn" type="submit" name="upload" value="Upload" />
    </form> 
    <?php
	if (isset($_POST['upload'])) {
		$namaFile = $_FILES['berkas']['name'];
		$namaSementara = $_FILES['berkas']['tmp_name'];
		$dirUpload = "upload/";

		$terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);

		if ($terupload) {
		    echo "Upload berhasil!<br/>";
		    echo "Link: <a href='".$dirUpload.$namaFile."'>".$namaFile."</a>";
		} else {
		    echo "Upload Gagal!";
			}
		}	
	?>
	</section>

	<footer>
		<p>User yang pernah login: <?php echo getClickCount(); ?></p>
		<div class="wrapper" style="text-align: center; height: em; width: 30%; margin: 1em auto;">
			<center><p  style="color: black; position: absolute; left: 50%; top: 37%;  transform: translate(-50%, -50%);"><?php echo persen()."%"; ?></p></center>
			<?php 
			echo '<div style="width: '.persen().'%; background-color: #48a9dc; height: 1.5em;">';
			 ?>
		</div></div>
	</footer>
</body>
</html>